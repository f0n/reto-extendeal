import {
  ADD_PRODUCT,
  Add,
  REMOVE_PRODUCT,
  Remove,
  UPDATE_PRODUCT,
  Update,
} from "./../../../redux/actions/productActions";
import ProductMock from "./../../../__mocks__/productMock";

describe("ProductActions", () => {
  test("ADD_PRODUCT", () => {
    const payload = ProductMock;
    const expected = {
      type: ADD_PRODUCT,
      payload,
    };
    expect(Add(payload)).toEqual(expected);
  });

  test("REMOVE_PRODUCT", () => {
    const payload = ProductMock;
    const expected = {
      type: REMOVE_PRODUCT,
      payload,
    };
    expect(Remove(payload)).toEqual(expected);
  });

  test("UPDATE_PRODUCT", () => {
    const payload = ProductMock;
    const expected = {
      type: UPDATE_PRODUCT,
      payload,
    };
    expect(Update(payload)).toEqual(expected);
  });
});
