import ProductReducers from "./../../../redux/reducers/product";
import ProductMock from "./../../../__mocks__/productMock";
import ProductsMock from "./../../../__mocks__/productsMock";
import {
  ADD_PRODUCT,
  REMOVE_PRODUCT,
  UPDATE_PRODUCT,
} from "./../../../redux/actions/productActions";

describe("Product Reducers", () => {
  test("retorno el estado incial", () => {
    expect(ProductReducers({}, "")).toEqual({});
  });

  test("ADD_PRODUCT", () => {
    const initialState = {
      product: [],
    };
    const payload = ProductMock;
    const action = {
      type: ADD_PRODUCT,
      payload,
    };
    const expected = {
      product: [ProductMock],
    };

    expect(ProductReducers(initialState, action)).toEqual(expected);
  });

  test("REMOVE_PRODUCT", () => {
    const initialState = {
      product: ProductsMock,
    };
    const payload = ProductMock;
    const action = {
      type: REMOVE_PRODUCT,
      payload,
    };
    const expected = {
      product: ProductsMock.filter((item) => item.id !== ProductMock.id),
    };

    expect(ProductReducers(initialState, action)).toEqual(expected);
  });

  test("UPDATE_PRODUCT", () => {
    const initialState = {
      product: [ProductMock],
    };
    let productMockUpdate = ProductMock;
    //cambio un valor
    productMockUpdate.price = 2000;

    const payload = productMockUpdate;
    const action = {
      type: UPDATE_PRODUCT,
      payload,
    };
    const expected = {
      product: [productMockUpdate],
    };

    expect(ProductReducers(initialState, action)).toEqual(expected);
  });
});
