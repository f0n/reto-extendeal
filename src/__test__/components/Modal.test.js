import React from "react";
import { mount, shallow } from "enzyme";
import Modal from "../../components/Modal";
describe("<Modal/>", () => {
  test("render del componente Modal", () => {
    const modal = mount(<Modal />);

    expect(modal.length).toEqual(1);
  });

  test("Prueba de boton cerrar", () => {
    const handleModalView = jest.fn();
    const wrapper = shallow(<Modal handleModalView={handleModalView} />);
    wrapper.find("modalstyled__BTN_CLOSE").simulate("click");
    expect(handleModalView).toHaveBeenCalledTimes(1);
  });

  test("prueba de renderizacion de props hijo", () => {
    const modal = mount(
      <Modal>
        <h1 id="title_test">title_test</h1>
      </Modal>
    );
    expect(modal.find("#title_test").text()).toEqual("title_test");
  });
});
