import React from "react";
import { mount, shallow } from "enzyme";
import Product from "./../../components/Product";
import ProductMock from "./../../__mocks__/productMock";
import ProviderMock from "../../__mocks__/providerMock";
describe("<Product/>", () => {
  test("render del componente Product", () => {
    const product = mount(<Product product={ProductMock} />);

    expect(product.length).toEqual(1);
  });

  test("render de product img", () => {
    const product = shallow(<Product product={ProductMock} />);
    expect(product.find("productstyled__IMG_PRODUCT").prop("src")).toEqual(
      ProductMock.img
    );
  });
  test("render de product name", () => {
    const product = shallow(<Product product={ProductMock} />);
    expect(product.find("productstyled__TITLE").text()).toEqual(
      ProductMock.name
    );
  });

  test("render de product descripcion", () => {
    const product = shallow(<Product product={ProductMock} />);
    expect(product.find("productstyled__DESCRIPTION").text()).toEqual(
      ProductMock.description
    );
  });

  test("render de product Price", () => {
    const product = shallow(<Product product={ProductMock} />);
    expect(product.find("productstyled__PRICE").text()).toEqual(
      `$${ProductMock.price}`
    );
  });
});
