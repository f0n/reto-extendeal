import React from "react";
import { mount, shallow } from "enzyme";
import Layout from "../../components/Layout";
import ProductMock from "./../../__mocks__/productMock";
import ProviderMock from "./../../__mocks__/providerMock";
describe("<Layout/>", () => {
  const layout = mount(
    <ProviderMock>
      <Layout />
    </ProviderMock>
  );
  test("render del componente Layout", () => {
    expect(layout.length).toEqual(1);
  });
});
