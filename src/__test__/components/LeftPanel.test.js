import React from "react";
import { mount, shallow } from "enzyme";
import LeftPanel from "./../../components/LeftPanel";
import ProviderMock from "./../../__mocks__/providerMock";

describe("<LeftPanel/>", () => {
  const leftPanel = mount(
    <ProviderMock>
      <LeftPanel />
    </ProviderMock>
  );
  test("render del componente LeftPanel", () => {
    expect(leftPanel.length).toEqual(1);
  });
});
