import React from "react";
import { mount, shallow } from "enzyme";
import Search from "./../../components/Search";
import ICON_SEARCH from "./../../assets/img/search_icon.webp";
describe("<Search/>", () => {
  test("renderizacion del componente Search", () => {
    const search = mount(<Search />);
    expect(search.length).toEqual(1);
  });

  test("renderizacion de img", () => {
    const wrapper = shallow(<Search />);
    expect(wrapper.find("searchstyled__IMG_ICON_SEARCH").prop("src")).toEqual(
      ICON_SEARCH
    );
  });
});
