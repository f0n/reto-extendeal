import React from "react";
import { mount } from "enzyme";
import { create } from "react-test-renderer";
import Footer from "../../components/Footer";
import "jest-styled-components";

describe("<Footer/>", () => {
  const footer = mount(<Footer />);
  test("render del componente footer", () => {
    expect(footer.length).toEqual(1);
  });
});

describe("footer snapshot", () => {
  test("comprobar la UI del componente footer", () => {
    const footer = create(<Footer />);
    expect(footer.toJSON()).toMatchSnapshot();
  });
});
