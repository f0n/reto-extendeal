import React from "react";
import { mount, shallow } from "enzyme";
import Details from "../../components/Details";
import ProductMock from "./../../__mocks__/productMock";

describe("<Details/>", () => {
  const details = shallow(<Details product={ProductMock} />);
  test("render del componente details", () => {
    expect(details.length).toEqual(1);
  });

  test("prueba de boton editar", () => {
    const handleModalView = jest.fn(); //activa el modal
    const wrapper = mount(
      <Details product={ProductMock} handleModalView={handleModalView} />
    );
    wrapper.find("#btn_edit").simulate("click");
    expect(handleModalView).toHaveBeenCalledTimes(1);
  });

  test("prueba de boton borrar", () => {
    const handleAlertChangeDelete = jest.fn(); //activa el sweet-alert
    const wrapper = mount(
      <Details
        product={ProductMock}
        handleAlertChangeDelete={handleAlertChangeDelete}
      />
    );
    wrapper.find("#btn_delete").simulate("click");
    expect(handleAlertChangeDelete).toHaveBeenCalledTimes(1);
  });
});
