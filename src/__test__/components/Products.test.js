import React from "react";
import { mount, shallow } from "enzyme";
import Products from "./../../components/Products";
import ProviderMocks from "./../../__mocks__/providerMock";
import ProductsMock from "./../../__mocks__/productsMock";
describe("<Products/>", () => {
  test("render del componente Products", () => {
    const products = mount(
      <ProviderMocks>
        <Products productList={ProductsMock} />
      </ProviderMocks>
    );
    expect(products.length).toEqual(1);
  });
});
