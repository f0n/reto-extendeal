import React from "react";
import { mount, shallow } from "enzyme";
import ProductForm from "./../../components/ProductForm";

describe("<ProductForm/> CREATE", () => {
  test("render del componente ProductForm", () => {
    const productForm = mount(<ProductForm product={{}} />);
    expect(productForm.length).toEqual(1);
  });

  test("render de boton", () => {
    const buttonText = "Crear Product";
    const productForm = shallow(
      <ProductForm product={{}} buttonText={buttonText} />
    );
    expect(productForm.find("productFormstyled__BUTTON").text()).toEqual(
      buttonText
    );
  });

  test("prueba de click en boton crear", () => {
    const btnPressFunction = jest.fn();
    const buttonText = "Crear Product";
    const wrapper = shallow(
      <ProductForm
        product={{}}
        buttonText={buttonText}
        btnPressFunction={btnPressFunction}
      />
    );
    wrapper.find("productFormstyled__BUTTON").simulate("click");
    expect(btnPressFunction).toHaveBeenCalledTimes(1);
  });
});
