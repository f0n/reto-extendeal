export const ADD_PRODUCT = "ADD_PRODUCT";
export const REMOVE_PRODUCT = "REMOVE_PRODUCT";
export const UPDATE_PRODUCT = "UPDATE_PRODUCT";

export const Add = (payload) => ({
  type: ADD_PRODUCT,
  payload,
});

export const Remove = (payload) => ({
  type: REMOVE_PRODUCT,
  payload,
});

export const Update = (payload) => ({
  type: UPDATE_PRODUCT,
  payload,
});
