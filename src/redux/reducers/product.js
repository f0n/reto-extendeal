import {
  ADD_PRODUCT,
  REMOVE_PRODUCT,
  UPDATE_PRODUCT,
} from "./../actions/productActions";
import product_initial_state from "./../../initialState";
const INITIAL_STATE = {
  product: product_initial_state,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_PRODUCT:
      return { product: [...state.product, action.payload] };
    case REMOVE_PRODUCT:
      return {
        ...state,
        product: state.product.filter((item) => item.id !== action.payload.id),
      };
    case UPDATE_PRODUCT:
      state.product[
        state.product.findIndex((item, i) => {
          return item.id === action.payload.id;
        })
      ] = action.payload;
      return state;
    default:
      return state;
  }
};
