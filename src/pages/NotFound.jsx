import React from "React";
import { CONTAINEAR, IMG, TITLE } from "./../styles/notFound.styled";
import { Link } from "react-router-dom";
const NotFound = () => {
  return (
    <React.Fragment>
      <CONTAINEAR>
        <TITLE>Page not found</TITLE>

        <Link to="/">
          <p>Volver a home</p>
        </Link>
      </CONTAINEAR>
    </React.Fragment>
  );
};
export default NotFound;
