import React from "react";
import ProductForm from "./../../components/ProductForm";
import SweetAlert from "sweetalert2-react";
import Sha1 from "sha1";
import { Add } from "./../../redux/actions/productActions";
import { connect } from "react-redux";
import { WRAPPER, CONTAINER } from "../../styles/productCreatePage.styled";
class CreateProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alert: {
        type: null,
        title: null,
        msj: null,
        status: false,
      },
      product: {
        id: null,
        name: null,
        description: null,
        price: null,
        img: null,
      },
    };
    this.handleChange = this.handleChange.bind(this);
    this.alertParamsIncomplet = this.alertParamsIncomplet.bind(this);
    this.alertProductAddSucess = this.alertProductAddSucess.bind(this);
  }
  handleChange = (e) => {
    e.preventDefault();

    this.setState({
      ...this.state,
      product: { ...this.state.product, [e.target.name]: e.target.value },
    });
  };

  handleDragAndDropImg = (File) => {
    this.setState({
      ...this.state,
      product: { ...this.state.product, img: URL.createObjectURL(File[0]) },
    });
  };

  alertParamsIncomplet = () => {
    this.setState({
      ...this.state,
      alert: {
        type: "warning",
        title: "Alerta!",
        msj: "Por favor complete todos los datos",
        status: true,
      },
    });
  };

  alertProductAddSucess = () => {
    this.setState({
      ...this.state,
      alert: {
        type: "success",
        title: "Correcto!",
        msj: "Se creo el producto existosamente!",
        status: true,
      },
    });
  };

  redierctToPageList() {
    this.props.history.push("/product/list");
  }

  createProduct = (e) => {
    e.preventDefault();

    let product = this.state.product;
    product.id = Sha1(Math.random());
    if (
      !product.id ||
      !product.name ||
      !product.description ||
      !product.price ||
      !product.img
    )
      this.alertParamsIncomplet();
    else {
      this.alertProductAddSucess();
      this.props.addProduct(product);
    }
  };
  render() {
    return (
      <React.Fragment>
        <CONTAINER>
          <WRAPPER>
            <ProductForm
              shadow
              title="Creacion de producto"
              buttonText="Crear producto"
              product={this.state.product}
              handleChange={this.handleChange}
              handleDragAndDropImg={this.handleDragAndDropImg}
              btnPressFunction={this.createProduct}
            />
          </WRAPPER>
        </CONTAINER>
        <SweetAlert
          show={this.state.alert.status}
          title={this.state.alert.title}
          text={this.state.alert.msj}
          type={this.state.alert.type}
          onConfirm={() => {
            this.setState({
              ...this.state,
              alert: { ...this.state.alert, status: false },
            });
            let product = this.state.product;
            if (
              product.id ||
              product.name ||
              product.description ||
              product.price ||
              product.img
            )
              this.redierctToPageList();
          }}
        />
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addProduct: (product) => dispatch(Add(product)),
  };
};
export default connect(null, mapDispatchToProps)(CreateProduct);
