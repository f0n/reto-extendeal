import React, { useState } from "react";
import { connect } from "react-redux";
import Products from "./../../components/Products";
import Search from "./../../components/Search";
import { useSelector } from "react-redux";
const ProductListPage = () => {
  const product = useSelector((props) => props.ProductReducers.product);
  const [productList, filterProductList] = useState(product);

  const handleSearch = (e) => {
    e.preventDefault();
    let filter = [];
    filter = product.filter(
      (item) =>
        item.name.toLowerCase().indexOf(e.target.value.toLowerCase()) > -1 || //filter name
        item.description.toLowerCase().indexOf(e.target.value.toLowerCase()) > //filter description
          -1
    );
    filterProductList(filter);
  };

  return (
    <React.Fragment>
      <Search handleSearch={handleSearch} />
      <Products productList={productList} />
    </React.Fragment>
  );
};

const mapStateToProps = ({ ProductReducers }) => {
  return { ProductReducers };
};
export default connect(mapStateToProps, {})(ProductListPage);
