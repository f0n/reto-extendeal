import React from "react";
import Modal from "./../../components/Modal";
import ProductForm from "./../../components/ProductForm";
import Details from "./../../components/Details";
import SweetAlert from "sweetalert2-react";
import { Update, Remove } from "./../../redux/actions/productActions";
import { connect } from "react-redux";

class DetailsProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalView: false,
      alert: {
        type: null,
        title: null,
        msj: null,
        status: false,
      },
      product: {
        id: null,
        name: null,
        description: null,
        price: null,
        img: null,
      },
    };
    this.handleModalView = this.handleModalView.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.alertParamsIncomplet = this.alertParamsIncomplet.bind(this);
    this.alertProductUpdateSucess = this.alertProductUpdateSucess.bind(this);
    this.handleAlertChangeDelete = this.handleAlertChangeDelete.bind(this);
  }

  componentDidMount() {
    let id = this.props.match.params.id;
    let { product } = this.props.ProductReducers;
    this.setState({
      ...this.state,
      product: product.filter((item) => item.id === id)[0],
    });
  }

  handleModalView = (e) => {
    this.setState({ ...this.state, modalView: e });
  };

  handleChange = (e) => {
    e.preventDefault();

    this.setState({
      ...this.state,
      product: { ...this.state.product, [e.target.name]: e.target.value },
    });
  };

  handleDragAndDropImg = (File) => {
    this.setState({
      ...this.state,
      product: { ...this.state.product, img: URL.createObjectURL(File[0]) },
    });
  };

  handleAlertChangeDelete = () => {
    this.setState({
      ...this.state,
      alert: {
        status: true,
        title: "question",
        msj: "Esta seguro que desea borrar el item seleccionado?",
        type: "question",
      },
    });
  };
  alertParamsIncomplet = () => {
    this.setState({
      ...this.state,
      alert: {
        type: "warning",
        title: "Alerta!",
        msj: "Por favor complete todos los datos",
        status: true,
      },
    });
  };

  alertProductUpdateSucess = () => {
    this.setState({
      ...this.state,
      alert: {
        type: "success",
        title: "Correcto!",
        msj: "Se actualizo el producto existosamente!",
        status: true,
      },
    });
  };

  updateProduct = (e) => {
    e.preventDefault();

    let product = this.state.product;
    if (
      !product.id ||
      !product.name ||
      !product.description ||
      !product.price ||
      !product.img
    )
      this.alertParamsIncomplet();
    else {
      this.alertProductUpdateSucess();
      this.props.updateProduct(product);
    }
  };

  deleteProduct = (e) => {
    this.props.removeProduct(this.state.product);
    this.props.history.push("/product/list");
  };

  render() {
    return (
      <React.Fragment>
        <Details
          product={this.state.product}
          handleModalView={this.handleModalView}
          handleAlertChangeDelete={this.handleAlertChangeDelete}
        />
        <Modal
          handleModalView={this.handleModalView}
          modalView={this.state.modalView}
        >
          <ProductForm
            title="Editar Producto"
            buttonText="Actualizar producto"
            product={this.state.product}
            handleChange={this.handleChange}
            handleDragAndDropImg={this.handleDragAndDropImg}
            btnPressFunction={this.updateProduct}
          />
        </Modal>
        <SweetAlert
          show={this.state.alert.status}
          title={this.state.alert.title}
          text={this.state.alert.msj}
          type={this.state.alert.type}
          showCancelButton={this.state.alert.type === "question" ? true : false}
          cancelButtonText="NO"
          confirmButtonText={this.state.alert.type === "question" ? "Si" : "OK"}
          onConfirm={() => {
            this.setState({
              ...this.state,
              modalView: false,
              alert: { ...this.state.alert, status: false },
            });

            if (this.state.alert.type === "question") {
              this.deleteProduct();
            }
          }}
          onCancel={() =>
            this.setState({
              ...this.state,
              alert: { ...this.state.alert, status: false },
            })
          }
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ ProductReducers }) => {
  return { ProductReducers };
};
const mapDispatchToProps = (dispatch) => {
  return {
    updateProduct: (product) => dispatch(Update(product)),
    removeProduct: (product) => dispatch(Remove(product)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DetailsProduct);
