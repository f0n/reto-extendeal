export default [
  {
    id: "fd92458f6937f94d38fa6cfa100f352be601ce08",
    name: "Hamburgesa",
    description: "hamburgesa completa",
    price: 240,
    img:
      "https://t2.rg.ltmcdn.com/es/images/0/8/9/img_pan_para_hamburguesa_28980_orig.jpg",
  },
  {
    id: "93967b958bb8582e51dd9a224b5fd9e5c3e45fc5",
    name: "Papas fritas",
    description: "para 2 personas",
    price: 80,
    img:
      "https://www.infobae.com/new-resizer/sLQil9kYIdkppDQUUpmcmRJhSCk=/1920x1079/filters:format(jpg):quality(100)//s3.amazonaws.com/arc-wordpress-client-uploads/infobae-wp/wp-content/uploads/2019/09/03181017/papas-fritas.jpg",
  },
  {
    id: "69689c373a22fddabbba26806d9e7963a7c6fd3d",
    name: "Pastas",
    description: "Pastas rellena de queso",
    price: 170,
    img:
      "https://e00-elmundo.uecdn.es/assets/multimedia/imagenes/2019/09/23/15692408990537.jpg",
  },
  {
    id: "b29f7898e2da4bab2aee2dfd81191e6030d4af4c",
    name: "Milanesa",
    description: "Milanesa napolitana",
    price: 150,
    img:
      "https://t2.rg.ltmcdn.com/es/images/4/9/8/img_milanesa_de_carne_11894_orig.jpg",
  },
  {
    id: "0a96279e254337436cf7427e86ad215ebef70792",
    name: "ñoquis",
    description: "Ñoquis con salsa blanca",
    price: 200,
    img:
      "https://elgourmet.s3.amazonaws.com/recetas/cover/b7d97329520320cb6893d991e2d9ccad_3_3_photo.png",
  },
  {
    id: "6e1ad377b6be11ac84672bb30bca68c3c4a7e433",
    name: "pizza",
    description: "Pizza muzarella",
    price: 180,
    img:
      "https://www.lavanguardia.com/files/og_thumbnail/uploads/2017/11/21/5e9977a58c570.jpeg",
  },
  {
    id: "371cd4bb57c85a727334b8b6e4250126ef6e7cf5",
    name: "Pescado",
    description: "pescado frito ",
    price: 100,
    img:
      "https://sevilla.abc.es/gurme/wp-content/uploads/sites/24/2014/06/receta-pescado-plancha.jpg",
  },
  {
    id: "80e54247e96089669c3cc7a0c7f87fbcdcd55d10",
    name: "Helado",
    description: "Helado de frutilla ",
    price: 50,
    img:
      "https://www.recetasderechupete.com/wp-content/uploads/2019/07/shutterstock_1010248351-525x360.jpg",
  },
];
