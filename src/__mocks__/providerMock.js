import React from "react";
import { Router } from "react-router-dom";
import { createBrowserHistory } from "history";

//redux
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import Reducers from "./../redux/reducers";
import reduxThunk from "redux-thunk";
const store = createStore(Reducers, {}, applyMiddleware(reduxThunk));

const history = createBrowserHistory();

const ProviderMock = (props) => {
  return (
    <React.Fragment>
      <Provider store={store}>
        <Router history={history}>{props.children}</Router>
      </Provider>
    </React.Fragment>
  );
};

export default ProviderMock;
