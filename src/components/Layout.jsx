import React from "react";
import Footer from "./Footer";
import LeftPanel from "./LeftPanel";
import { CONTAINER, CONTAINER_CHILDREN } from "./../styles/layout.styled";

const Layout = (props) => {
  return (
    <React.Fragment>
      <CONTAINER>
        <LeftPanel />
        <CONTAINER_CHILDREN>
          {props.children}

          <Footer />
        </CONTAINER_CHILDREN>
      </CONTAINER>
    </React.Fragment>
  );
};

export default Layout;
