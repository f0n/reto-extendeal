import React from "react";
import {
  CONTAINER,
  GRID,
  IMG_LOGO_COMPANY,
  SEPARATOR_HORIZONTAL,
  MENU_CONTAINER,
  CENTER_IMG,
  LINK,
  CONTAINER_RESOURCE,
  CENTER_VERTICAL,
} from "./../styles/leftPanel.styled";
import LOGO_COMPANY from "./../assets/img/logoCompany.webp";

const LeftPanel = () => {
  const resources = ["list", "create"];
  return (
    <>
      <CONTAINER>
        <GRID>
          <CENTER_IMG>
            <LINK to="/">
              <IMG_LOGO_COMPANY alt="img_logo_company" src={LOGO_COMPANY} />
            </LINK>
          </CENTER_IMG>
          <MENU_CONTAINER>
            <SEPARATOR_HORIZONTAL />
            {resources.map((resource, key) => {
              return (
                <CONTAINER_RESOURCE key={key}>
                  <CENTER_VERTICAL>
                    <LINK to={`/product/${resource}`}>{resource}</LINK>
                  </CENTER_VERTICAL>
                </CONTAINER_RESOURCE>
              );
            })}
          </MENU_CONTAINER>
        </GRID>
      </CONTAINER>
    </>
  );
};

export default LeftPanel;
