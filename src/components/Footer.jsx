import React from "react";
import {
  CONTAINER,
  CENTER,
  GRID,
  CENTER_IMG,
  CONTACT_GRID,
  TITLE,
  CONTACT_INFO,
  LABEL,
  TEXT,
  GRID_SOCIAL_MEDIA,
  GRID_SOCIAL_MEDIA_IMG,
  IMG_SOCIAL_MEDIA,
} from "./../styles/footer.styled";
import { IMG_LOGO_COMPANY } from "./../styles/leftPanel.styled";
import LOGO_COMPANY from "./../assets/img/logoCompany.webp";
import IMG_GITHUB from "./../assets/img/github_social_media.webp";
import IMG_LINKEDIN from "./../assets/img/linkedin_social_media.webp";
import IMG_GITLAB from "./../assets/img/gitlab_social_media.webp";
const Footer = () => {
  return (
    <>
      <CONTAINER>
        <CENTER>
          <GRID>
            <CENTER_IMG>
              <IMG_LOGO_COMPANY alt="img_logo_company" src={LOGO_COMPANY} />
            </CENTER_IMG>
            <CONTACT_GRID>
              <TITLE>Contact</TITLE>
              <CONTACT_INFO>
                <LABEL>Email:</LABEL>
                <TEXT>angel_la12@hotmail.com.ar</TEXT>
                <LABEL>Phone:</LABEL>
                <TEXT>+54 9 11-6551-2378</TEXT>
              </CONTACT_INFO>
            </CONTACT_GRID>
            <GRID_SOCIAL_MEDIA>
              <TITLE>Social media</TITLE>
              <GRID_SOCIAL_MEDIA_IMG>
                <CENTER_IMG>
                  <a href="https://github.com/Angelf0ne01">
                    <IMG_SOCIAL_MEDIA alt="img_github" src={IMG_GITHUB} />
                  </a>
                </CENTER_IMG>
                <CENTER_IMG>
                  <a href="https://gitlab.com/f0n">
                    <IMG_SOCIAL_MEDIA alt="gitlab_linkedin" src={IMG_GITLAB} />
                  </a>
                </CENTER_IMG>
                <CENTER_IMG>
                  <a href="https://www.linkedin.com/in/angel-vazquez-b1543b151/">
                    <IMG_SOCIAL_MEDIA alt="img_linkedin" src={IMG_LINKEDIN} />
                  </a>
                </CENTER_IMG>
              </GRID_SOCIAL_MEDIA_IMG>
            </GRID_SOCIAL_MEDIA>
          </GRID>
        </CENTER>
      </CONTAINER>
    </>
  );
};

export default Footer;
