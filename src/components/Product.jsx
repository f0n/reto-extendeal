import React from "react";
import {
  CONTAINER,
  GRID,
  IMG_PRODUCT,
  CENTER_IMG,
  WRAPPER,
  TITLE,
  DESCRIPTION,
  PRICE,
} from "./../styles/product.styled";
import IMG_NOT_FOUND from "./../assets/img/img_not_found.webp";

const Product = ({ product }) => {
  return (
    <React.Fragment>
      <CONTAINER>
        <WRAPPER>
          <GRID>
            <CENTER_IMG>
              <IMG_PRODUCT
                alt="img_product"
                src={product.img || IMG_NOT_FOUND}
              />
            </CENTER_IMG>
            <TITLE>{product.name}</TITLE>
            <DESCRIPTION>{product.description}</DESCRIPTION>
            <PRICE>{`$${product.price || ""}`}</PRICE>
          </GRID>
        </WRAPPER>
      </CONTAINER>
    </React.Fragment>
  );
};

export default Product;
