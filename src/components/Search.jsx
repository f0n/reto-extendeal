import React from "react";

import {
  CONTAINER,
  SEARCH_WRAPPER,
  SEARCH,
  CENTER_SEARCH,
  IMG_ICON_SEARCH,
  CENTER_IMG,
} from "./../styles/search.styled";

import ICON_SEARCH from "./../assets/img/search_icon.webp";
const Search = (props) => {
  return (
    <>
      <CONTAINER>
        <CENTER_SEARCH>
          <SEARCH_WRAPPER>
            <SEARCH
              type="text"
              placeholder="Search..."
              onChange={props.handleSearch}
            />
            <CENTER_IMG>
              <IMG_ICON_SEARCH alt="search_icon" src={ICON_SEARCH} />
            </CENTER_IMG>
          </SEARCH_WRAPPER>
        </CENTER_SEARCH>
      </CONTAINER>
    </>
  );
};

export default Search;
