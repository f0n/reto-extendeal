import React from "react";
import {
  CONTAINER,
  CENTER_IMG,
  IMG_PRODUCT,
  GRID_PRODUCT_INFO,
  TITLE,
  CENTER_TITLE,
  GRID_BTN,
  CENTER_BTN,
  TEXT_DESCRIPTION,
  CENTER_V,
  GRID,
  PRICE,
  SVG,
} from "./../styles/details.styled";
const Details = ({ product, handleModalView, handleAlertChangeDelete }) => {
  return (
    <>
      <CONTAINER>
        <GRID>
          <CENTER_IMG>
            <IMG_PRODUCT alt="img_product" src={product.img} />
          </CENTER_IMG>
          <GRID_PRODUCT_INFO>
            <CENTER_TITLE>
              <TITLE>{product.name}</TITLE>
            </CENTER_TITLE>
            <CENTER_V>
              <TEXT_DESCRIPTION>{product.description}</TEXT_DESCRIPTION>
            </CENTER_V>
            <CENTER_V>
              <PRICE>{`$ ${product.price}`}</PRICE>
            </CENTER_V>
            {/* btn */}
            <GRID_BTN>
              <CENTER_BTN>
                <button
                  id="btn_edit"
                  style={{ backgroundColor: "white", border: "0px" }}
                  type="button"
                  onClick={() => handleModalView(true)}
                >
                  <SVG
                    xmlns="http://www.w3.org/2000/svg"
                    width="48"
                    height="48"
                    viewBox="0 0 48 48"
                  >
                    <path d="M6 34.5V42h7.5l22.13-22.13-7.5-7.5L6 34.5zm35.41-20.41c.78-.78.78-2.05 0-2.83l-4.67-4.67c-.78-.78-2.05-.78-2.83 0l-3.66 3.66 7.5 7.5 3.66-3.66z" />
                  </SVG>
                </button>
              </CENTER_BTN>
              <CENTER_BTN>
                <button
                  style={{ backgroundColor: "white", border: "0px" }}
                  id="btn_delete"
                  type="button"
                  onClick={() => handleAlertChangeDelete(true)}
                >
                  <SVG
                    red
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 48 48"
                  >
                    <path d="M12 38c0 2.21 1.79 4 4 4h16c2.21 0 4-1.79 4-4V14H12v24zM38 8h-7l-2-2H19l-2 2h-7v4h28V8z" />
                  </SVG>
                </button>
              </CENTER_BTN>
            </GRID_BTN>
          </GRID_PRODUCT_INFO>
        </GRID>
      </CONTAINER>
    </>
  );
};

export default Details;
