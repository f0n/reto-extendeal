import React from "react";
import { CONTAINER, LIST } from "./../styles/products.styled";
import Product from "./Product";
import { Link } from "react-router-dom";
const Products = ({ productList }) => {
  return (
    <React.Fragment>
      <CONTAINER>
        <LIST>
          {productList.map((item, key) => {
            return (
              <Link to={`/product/details/${item.id}`} key={key}>
                <Product product={item} />
              </Link>
            );
          })}
        </LIST>
      </CONTAINER>
    </React.Fragment>
  );
};

export default Products;
