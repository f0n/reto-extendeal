import React from "react";
import {
  CONTAINER,
  WRAPPER,
  FORM,
  TITLE,
  INPUT,
  BUTTON,
  IMG_SECTION,
  SVG_UPLOAD,
  TEXT_IMG,
} from "./../styles/productForm.styled";

import { IMG_PRODUCT } from "./../styles/product.styled";
import Dropzone from "react-dropzone";

const ProductForm = ({
  product,
  shadow,
  handleChange,
  title,
  handleDragAndDropImg,
  buttonText,
  btnPressFunction,
}) => {
  return (
    <React.Fragment>
      <CONTAINER>
        <WRAPPER shadow={shadow}>
          <TITLE>{title}</TITLE>
          <FORM>
            <Dropzone onDrop={(File) => handleDragAndDropImg(File)}>
              {({ getRootProps, getInputProps }) => (
                <IMG_SECTION>
                  <div {...getRootProps()}>
                    <input {...getInputProps()} />
                    {product.img ? (
                      <IMG_PRODUCT src={product.img} alt="img_product_upload" />
                    ) : (
                      <div>
                        <SVG_UPLOAD viewBox="0 0 48 48">
                          <path d="M38.71 20.07C37.35 13.19 31.28 8 24 8c-5.78 0-10.79 3.28-13.3 8.07C4.69 16.72 0 21.81 0 28c0 6.63 5.37 12 12 12h26c5.52 0 10-4.48 10-10 0-5.28-4.11-9.56-9.29-9.93zM28 26v8h-8v-8h-6l10-10 10 10h-6z" />
                        </SVG_UPLOAD>
                        <TEXT_IMG>Elija una img</TEXT_IMG>
                      </div>
                    )}
                  </div>
                </IMG_SECTION>
              )}
            </Dropzone>
            <INPUT
              type="text"
              placeholder="Name"
              name="name"
              defaultValue={product.name}
              onChange={handleChange}
            />
            <INPUT
              type="text"
              placeholder="Description"
              name="description"
              defaultValue={product.description}
              onChange={handleChange}
            />
            <INPUT
              type="number"
              placeholder="Price"
              name="price"
              defaultValue={product.price}
              onChange={handleChange}
            />
            <BUTTON onClick={btnPressFunction}>{buttonText}</BUTTON>
          </FORM>
        </WRAPPER>
      </CONTAINER>
    </React.Fragment>
  );
};

export default ProductForm;
