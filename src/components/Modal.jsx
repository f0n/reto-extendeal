import React from "react";
import {
  CONTAINER,
  MODAL_CONTENT,
  BTN_CLOSE,
  CENTER,
  WRAPPER,
} from "./../styles/modal.styled";
const Modal = (props) => {
  return (
    <React.Fragment>
      <CONTAINER view={props.modalView}>
        <WRAPPER>
          <MODAL_CONTENT>
            <BTN_CLOSE onClick={() => props.handleModalView(false)}>
              &times;
            </BTN_CLOSE>
            <CENTER>{props.children}</CENTER>
          </MODAL_CONTENT>
        </WRAPPER>
      </CONTAINER>
    </React.Fragment>
  );
};

export default Modal;
