import styled from "styled-components";

export const CONTAINER = styled.div`
  width: 100%;
  height: 100vh;
  display: grid;
  grid-template-columns: 20% 80%;
  grid-template-rows: 100%;
  background-color: #f9f9fb;

  /* phone */
  @media screen and (max-width: 768px) {
    height: 10vh;
    display: grid;
    grid-template-columns: 100%;
    grid-template-rows: 100%;
  }
`;

export const CONTAINER_CHILDREN = styled.div`
  width: 100%;
  height: 100vh;
  overflow-y: auto;
`;
