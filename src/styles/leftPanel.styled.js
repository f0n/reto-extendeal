import styled from "styled-components";
import { Link } from "react-router-dom";

export const CONTAINER = styled.div`
  width: 100%;
  height: 100vh;
  background-color: white;
  z-index: 1;
  box-shadow: 2px 2px 3px #8080801a;
  @media screen and (max-width: 768px) {
    width: 100%;
    height: 10vh;
  }
`;

export const GRID = styled.div`
  width: 100%;
  height: 100vh;
  display: grid;
  grid-template: 100%;
  grid-template-rows: 20% 80%;
  @media screen and (max-width: 768px) {
    height: 10vh;

    display: grid;
    grid-template-columns: 20% 80%;
    grid-template-rows: 100%;
  }
`;

/* img company */

export const IMG_LOGO_COMPANY = styled.img`
  height: 100px;
  @media screen and (max-width: 768px) {
    height: 10vh;
  }
`;

/* end img company */

export const SEPARATOR_HORIZONTAL = styled.hr`
  width: 90%;
  margin-left: 5%;
  margin-right: 5%;
  height: 1px;
  background-color: #80808029;
  border: none;
  @media screen and (max-width: 768px) {
    display: none;
  }
`;

/* list menu left */

export const MENU_CONTAINER = styled.div`
  float: left;
  @media screen and (max-width: 768px) {
    display: flex;
  }
`;

export const CENTER_IMG = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;
/* RESOURCES */
export const LINK = styled(Link)`
  text-decoration: none;
  padding: 20px;
  color: #333;
  font-size: 19px;
  width: 100%;
  @media screen and (max-width: 768px) {
    text-align: center;
    padding: 0px;
    padding-right: 30px;
  }
`;
export const CONTAINER_RESOURCE = styled.div`
  width: 100%;
  height: 40px;
  border-bottom: 1px solid #80808029;
  :hover {
    border-left: 10px solid #333;
  }

  @media screen and (max-width: 768px) {
    border-bottom: 0px;
    border-left: 1px solid #80808029;
    border-right: 1px solid #80808029;
  }
`;

export const CENTER_VERTICAL = styled.div`
  display: flex;
  width: 100%;
  height: 40px;
  align-items: center;
  @media screen and (max-width: 768px) {
    display: flex;
    width: 100%;
    height: 10vh;
    align-items: center;
    justify-content: center;
  }
`;
