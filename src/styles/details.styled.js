import styled from "styled-components";

export const CONTAINER = styled.div`
  width: 90%;
  height: 400px;
  padding: 40px;

  @media screen and (max-width: 768px) {
    padding: 0px;
    width: 100%;
    height: 650px;
    background-color: #bebebe;
  }
`;

export const GRID = styled.div`
  background-color: white;
  display: grid;
  grid-template-columns: 70% 30%;
  grid-template-rows: 100%;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.3);

  @media screen and (max-width: 768px) {
    display: grid;
    grid-template-columns: 100%;
    grid-template-rows: 70% 30%;
  }
`;

/*img */
export const CENTER_IMG = styled.div`
  height: 400px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const IMG_PRODUCT = styled.img`
  height: 300px;
  @media screen and (max-width: 768px) {
    width: 100%;
  }
`;
/* BTN DELETE AND  EDIT */
export const GRID_BTN = styled.div`
  display: grid;
  grid-template-columns: 50% 50%;
  grid-template-rows: 100%;
`;

export const CENTER_BTN = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const SVG = styled.svg`
  fill: #ccc;
  height: 20px;
  width: 20px;

  :hover {
    ${(props) => (props.red ? `fill:#CC0000;` : `fill:#0099CC;`)}
  }
`;

/* Product Information */
export const GRID_PRODUCT_INFO = styled.div`
  display: grid;
  grid-template-columns: 100%;
  grid-template-rows: 50% 15% 15% 20%;
  border-left: 1px solid #ddd;
  @media screen and (max-width: 768px) {
    border-left: 0px;
  }
`;

export const TITLE = styled.h1`
  color: #333;
  font-weight: bold;
  text-align: center;
`;

export const CENTER_TITLE = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
`;

export const PRICE = styled.h1`
  color: #333;
  font-weight: bold;
  text-align: center;
  padding-left: 40px;
  @media screen and (max-width: 768px) {
    padding: 0px;
  }
`;
/*description */

export const CENTER_V = styled.div`
  display: flex;
  align-items: center;
  @media screen and (max-width: 768px) {
    justify-content: center;
  }
`;
export const TEXT_DESCRIPTION = styled.p`
  color: #999;
  padding-left: 40px;
  @media screen and (max-width: 768px) {
    padding: 0px;
  }
`;

export const BTN = styled.button`
  background-color: white;
  border: 0px;
`;
