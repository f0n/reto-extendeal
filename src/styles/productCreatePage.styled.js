import styled from "styled-components";

export const CONTAINER = styled.div`
  width: 100%;
  height: 80vh;
  display: flex;
  justify-content: center;
  align-items: center;

  @media screen and (max-width: 740px) {
    height: 100vh;
  }
`;

export const WRAPPER = styled.div`
  width: 80%;
  @media screen and (max-width: 740px) {
    height: 100vh;
  }
`;
