import styled from "styled-components";

export const CONTAINER = styled.div`
  width: 220px;
  height: 270px;
  padding: 12px;
  float: left;
`;

export const WRAPPER = styled.div`
  width: 200px;
  height: 250px;
  border-radius: 4px;
  padding: 10px;
  background-color: #ffffff;
  box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.1);
  :hover {
    box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.1);
  }
`;

export const GRID = styled.div`
  width: 100%;
  height: 240px;
  display: grid;
  grid-template-columns: 100%;
  grid-template-rows: 70% 15% 8% 7%;
`;

//data of product.
/*img*/

export const CENTER_IMG = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;
export const IMG_PRODUCT = styled.img`
  width: 200px;
`;

/* end img */

/* TITLE*/
export const TITLE = styled.h1`
  text-align: center;
  color: #666;
  font-size: 17px;
  margin-top: 5px;
`;
/* end title */

export const DESCRIPTION = styled.span`
  color: #999;
  font-size: 15px;
  text-align: center;
`;

export const PRICE = styled.span`
  color: #333;
  font-size: 19px;
  text-align: center;
  font-weight: bold;
`;
