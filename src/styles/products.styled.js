import styled from "styled-components";
export const CONTAINER = styled.div`
  width: 100%;
`;
export const LIST = styled.div`
  padding: 50px;
  :after {
    content: "";
    display: table;
    clear: both;
  }

  @media screen and (max-width: 768px) {
    width: 65%;
    align-items: center;
  }
`;
