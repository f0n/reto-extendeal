import styled from "styled-components";

export const CONTAINER = styled.div`
  height: 90vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const WRAPPER = styled.div`
  width: 100%;
  height: 400px;
  padding: 30px;
  background-color: white;
  border-radius: 5px;
  ${(props) => (props.shadow ? `box-shadow: 2px 1px 20px 0px #8080805e;` : ``)}

  @media screen and (max-width: 768px) {
    width: 90%;
  }
`;

export const FORM = styled.form`
  width: 100%;
  height: 380px;
`;

export const TITLE = styled.h1`
  text-align: center;
  color: #333;
`;

export const INPUT = styled.input`
  padding: 10px 0px;
  width: 100%;
  border: 0px;
  border-bottom: 1px solid #ccc;
  margin-bottom: 10px;
  margin-top: 10px;

  ::placeholder {
    padding-left: 20px;
  }
`;

export const BUTTON = styled.button`
  width: 100%;
  padding: 20px;
  border: 0px;
  border-bottom: 4px solid #ccc;
  background-color: #333;
  border-radius: 4px;
  color: white;
  font-size: 14px;

  :hover {
    background-color: #333333e3;
  }
`;

export const IMG_SECTION = styled.section`
  background-color: #333;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 5px;
`;
export const TEXT_IMG = styled.p`
  color: white;
  text-align: center;
`;

export const SVG_UPLOAD = styled.svg`
  width: 48px;
  height: 48px;
  fill: #fff;
  margin: 20px;
  :hover {
    fill: #999;
  }
`;
