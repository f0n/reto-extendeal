import styled from "styled-components";

export const CONTAINER = styled.div`
  width: 100%;
  height: 300px;
  background-color: white;
  bottom: 0px;
  @media screen and (max-width: 768px) {
    height: 600px;
  }
`;
export const CENTER = styled.div`
  width: 100%;
  height: 300px;

  display: flex;
  justify-content: center;
  align-items: center;
  @media screen and (max-width: 768px) {
    height: 600px;
  }
`;
export const GRID = styled.div`
  display: grid;
  width: 90%;
  height: 100px;
  grid-template-columns: 20% 40% 40%;
  grid-template-rows: 100px;
  @media screen and (max-width: 768px) {
    height: 500px;
    grid-template-columns: 100%;
    grid-template-rows: 20% 40% 40%;
  }
`;

/*img */
export const CENTER_IMG = styled.div`
  width: 100%;
  height: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
/* end img */

/* contact grid */

export const CONTACT_GRID = styled.div`
  display: grid;
  grid-template-columns: 100%;
  grid-template-rows: 20% 80%;
`;

export const TITLE = styled.h3`
  color: #333;
  text-align: center;
`;

export const CONTACT_INFO = styled.div`
  float: left;
`;

export const LABEL = styled.div`
  color: #333;
  font-weight: bold;
  margin-top: 10px;
  @media screen and (max-width: 768px) {
    text-align: center;
  }
`;

export const TEXT = styled.p`
  color: #999;
  margin-left: 5px;
  font-size: 15px;
  @media screen and (max-width: 768px) {
    text-align: center;
  }
`;

/* social media */
export const GRID_SOCIAL_MEDIA = CONTACT_GRID;

export const GRID_SOCIAL_MEDIA_IMG = styled.div`
  display: grid;
  grid-template-columns: 33% 33% 33%;
  grid-template-rows: 100%;
`;

export const IMG_SOCIAL_MEDIA = styled.img`
  height: 50px;
`;
