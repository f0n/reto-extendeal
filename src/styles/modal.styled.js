import styled from "styled-components";

export const CONTAINER = styled.div`
  ${(props) => (props.view ? `display:block;` : `display:none;`)}
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0, 0, 0); /* Fallback color */
  background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
`;

export const WRAPPER = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const MODAL_CONTENT = styled.div`
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 40%;
  height: 80vh;
  float: left;

  @media screen and (max-width: 768px) {
    width: 80%;
    height: 90vh;
  }
`;

export const BTN_CLOSE = styled.span`
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
  :hover {
    color: #000;
    text-decoration: none;
    cursor: pointer;
  }
`;

export const CENTER = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
