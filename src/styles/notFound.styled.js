import styled from "styled-components";

export const CONTAINEAR = styled.div`
  height: 100vh;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: white;
`;

export const IMG = styled.img`
  width: 50%;
`;

export const TITLE = styled.h1`
  z-index: 1;
  color: #333;
`;
