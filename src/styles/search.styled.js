import styled from "styled-components";

export const CONTAINER = styled.div`
  width: 100%;
  height: 120px;
  background-color: #f9f9fb;
`;
export const CENTER_SEARCH = styled.div`
  height: 120px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const SEARCH_WRAPPER = styled.div`
  display: grid;
  height: 50px;
  width: 70%;
  grid-template-columns: 90% 10%;
  grid-template-rows: 100%;
  background-color: white;
  border: 1px solid #ebebeb;
  border-radius: 5px;
  @media screen and (max-width: 768px) {
    width: 90%;
    grid-template-columns: 80% 20%;
  }
`;

export const SEARCH = styled.input`
  width: 100%;
  border: 0px;
  padding-left: 20px;
  font-size: 15px;
  color: #333;
  :focus {
    outline: none !important;

    border: 0px;
    border-bottom: 2px solid #999;
  }
  ::placeholder {
    color: #666;
    font-size: 20px;
    font-weight: bold;
  }
`;

/* img */
export const CENTER_IMG = styled.div`
  width: 100%;
  height: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 5px;
`;
export const IMG_ICON_SEARCH = styled.img`
  height: 30px;
`;
