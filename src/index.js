import React from "react";
import ReactDOM from "react-dom";
import Routers from "./routers/Router";

//redux
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import Reducers from "./redux/reducers";
import reduxThunk from "redux-thunk";

const store = createStore(Reducers, {}, applyMiddleware(reduxThunk));

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Routers />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
