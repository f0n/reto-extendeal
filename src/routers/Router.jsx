import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import ProductListPage from "./../pages/product/ListPage";
import ProductCreatePage from "./../pages/product/CreatePage";
import ProductDetailsPage from "./../pages/product/DetailsPage";
//import NotFoundPage from "./../pages/NotFound";

import Layout from "./../components/Layout";
const Router = () => {
  return (
    <React.Fragment>
      <BrowserRouter>
        <Layout>
          <Switch>
            <Route exact path="/" component={ProductListPage} />
            <Route exact path="/product/list" component={ProductListPage} />
            <Route exact path="/product/create" component={ProductCreatePage} />
            <Route
              exact
              path="/product/details/:id"
              component={ProductDetailsPage}
            />
          </Switch>
        </Layout>
      </BrowserRouter>
    </React.Fragment>
  );
};

export default Router;
