# Reto extendeal

## Front-end

![](https://scontent.faep5-1.fna.fbcdn.net/v/t1.0-9/10660334_1532957313607007_1704276022128422631_n.jpg?_nc_cat=105&_nc_oc=AQm7lu7CZMw7jRy1-e2_yxyTZ7HGsQS-HAyiLnbAnV7UudZqu1LGzQk_jXY9IOD2jbzRix_6EqH5CuGssXXERY6B&_nc_ht=scontent.faep5-1.fna&oh=78578257f761294d6b1537fc9f17e706&oe=5E0256F8)

##### Tecnologias Usadas!

![](https://www.wildcodeschool.com/uploads/b33f765c45049c6b04d099818ea37d11.png-What%20is%20React.js%20and%20why%20you%20should%20use%20it.png)
[React Documentacion](https://es.reactjs.org/docs/getting-started.html "React Documentacion")
![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/Redux_Logo.png/1200px-Redux_Logo.png)
[Redux Documentacion](https://es.redux.js.org/ "Redux Documentacion")
![](https://i.blogs.es/b5ae0b/1_q26gw-knzoxuqzkrr04t-g/1366_2000.png)
[Jest Documentacion](https://jestjs.io/docs/en/getting-started "Jest Documentacion")
#Pagina de demo.
[Demo-page](https://reto-extendeal-six.vercel.app/ "Demo-page")

[![pipeline status](https://gitlab.com/f0n/reto-extendeal/badges/master/pipeline.svg)](https://gitlab.com/f0n/reto-extendeal/-/commits/master)

### Requisitos!

- node.js
  [Instalar NodeJs](http://https://nodejs.org/es/download/ "Instalar NodeJs")

- puerto 3000 (tcp) verficar si esta abierto en la regla de firewall

> Si no sabe agregar una regla a su sistema operativo se le recomiendo desabilitar el firewall.

###### Verificacion de firewall.

> Utilizando Telnet.

```bash
telnet <ip> 3000
```

### Firewall

##### Windows

1. Panel de control
2. Firewall de Windows Defender
3. Configuracion Avanzada

###### Regla de entrada / Salida (Es lo mismo para los 2)

1. Nueva regla de entrada / Salida
2. Seleccionar Puerto
3. Seleccionar TCP
4. Seleccionar "Puerto locales espeficico"
5. Ingresar el puerto 3000
6. Permitir la conexion.
7. Seleccionar los Dominio, Privado y Publico.
8. Darle un nombre a la regla.

##### Linux

> Instalacion de firewall UFW

```bash
sudo apt-get install ufw
```

> Abro el puerto 3000

```bash
sudo ufw allow 3000
```

> Reinicio del firewall

```bash
sudo service ufw restart
```

# Instalacion

> 1. Descargar o clonar el repositorio.

```bash
     git clone https://gitlab.com/f0n/reto-extendeal.git
```

> 2)Mover a la carpeta raiz del proyecto.

```bash
	cd ./reto-extendeal
```

> 3. Instalar las dependecias necesarias.

```bash
npm install
```

> 3. Ejecutar el servidor.

```bash
npm start
```

# Test

El proyecto cuenta con una suit de test unitarios, el cual puede ejecutar con los siguientes comandos:

> Ejecutar Suit de test.

```bash
	npm test
```

> Ejecutar Test en modo Watch.

```bash
npm run test:watch
```

> Ejecutar un informe de la cobertura de los test (coverage)

```bash
 npm run test:coverage
```

> En el caso de hacer una actualizacion de las snapshot.

```bash
npm run test:update
```
